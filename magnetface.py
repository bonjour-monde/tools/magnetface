
import numpy as np
import math
from svgpathtools import *
import lxml.etree as ET
# import fontforge

## distance calculator
def calculateDistance(x1,y1,x2,y2):
     dist = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
     return dist

## find glyph fn
def findGlyph(glyph_name, svg):
    if glyph_name is None:
        return svg.findall(".//{http://www.w3.org/2000/svg}glyph")

    else:
        arr = []
        for glyph in glyph_name:
            letter = svg.findall(".//{http://www.w3.org/2000/svg}glyph[@glyph-name='"+glyph+"']")
            arr.append( letter[0] )
        return arr

## some functions to break the fonts
#######
def unCurveHard(paths):
    # getting rid of curves - hardway
    new_arr = []
    for path in paths:
        new_path = Path()
        for seg in path:
            line = Line(start=seg.start,end=seg.end)
            new_path.append(line)
        new_arr.append(new_path)
    return new_arr

# def unCurveSoft(path, cut):
#     # getting rid of curves with incrementation
#     new_path = Path()
#     tmp_path = []
#     for seg in path:
#         bezier_path = Path(seg)
#         points_array = []
#         for i in range(cut):
#             points_array.append(bezier_path.point(i/(cut-1)))
#         tmp_path.append(points_array)
#
#     for idx, point_group in enumerate(tmp_path):
#         for idx, point in enumerate(point_group):
#             if idx+1 < len(point_group):
#                 line = Line(start=point,end=point_group[idx+1])
#                 new_path.append(line)
#     return new_path

#### magnet Font
#######
def magnetFontarr(paths, glyph, gridSize, svg):
    if glyph.get("horiz-adv-x") != None:
        letter_width = glyph.get("horiz-adv-x")
    else:
        f = svg.find(".//{http://www.w3.org/2000/svg}font")
        letter_width = f.get("horiz-adv-x")
        # print()
    ##if glyph.get("horiz-adv-x") != None:
    ## init magnet
    #####
    grid_points = []
    arr_grid = []
    font_face = svg.findall(".//{http://www.w3.org/2000/svg}font-face")
    descent = abs(int(font_face[0].get("descent")))
    ascent = abs(int(font_face[0].get("ascent")))-260 ## note : recalculate letter box properly this is a qd fix
    font_height = int(font_face[0].get("cap-height"))+descent+ascent
    letter_width = int(letter_width)
    interval_x = letter_width/gridSize
    interval_y = font_height/gridSize
    radius = round(math.sqrt(np.square(interval_x)+np.square(interval_y))/2)
    ## define the grid
    #####
    for i in range(0, gridSize):
        for j in range(0, gridSize):
            x = (interval_x)*i+(interval_x)/2;
            y = ((interval_y)*j+(interval_y)/2)-descent;
            if x <= letter_width and y <= font_height:
                point = [x,y]
                grid_points.append(point)
    arr_tmp = np.array(grid_points)
    arr_tmp = arr_tmp.astype(float).view(np.complex128)

    for elem in arr_tmp.tolist():
            arr_grid.append(elem[0])

    ########
    ## transform the curves into a serie of line
    new_arr = []
    cut = 80
    for path in paths:
        tmp_path = []
        for seg in path:

            points_array = []
            for i in range(cut):
                point = seg.point(i/(cut-1))
                min_dic = {
                    'dist':400,
                    'points': '(80+80j)',
                }
                for grid_pt in arr_grid:
                    dist = calculateDistance(grid_pt.real, grid_pt.imag, point.real, point.imag )
                    if dist <= radius:
                        if dist < min_dic['dist'] :
                            min_dic['points'] = grid_pt
                            min_dic['dist'] = dist
                if min_dic['points'] != '(80+80j)':
                    points_array.append(min_dic['points'])

            ## remove duplicate points
            ######
            arr_clean = sorted(set(points_array), key=points_array.index)
            tmp_path.append(arr_clean)

        points_array = []
        for points in tmp_path:
            for point in points:
                points_array.append(point)

        final_points = sorted(set(points_array), key=points_array.index)

        ## redraw the path
        ######
        new_path = Path()
        for idx, point in enumerate(final_points):
            if idx+1 < len(final_points):
                if isinstance(point, complex):
                    line = Line(start=point,end=final_points[idx+1])
                    new_path.append(line)
            if idx == len(final_points)-1:
                if isinstance(point, complex):
                    line = Line(start=point,end=final_points[0])
                    new_path.append(line)

        new_arr.append(new_path)
    return new_arr

## twist for hyx
magnet_array = [7,9,12,20]

def processFont( letter, iteration ):

    for i in range(0,iteration):
        ## get svg data
        #### note : find another method to create a global svg to not call it all the time
        #### quick n dirty fix
        xml = ET.parse('Stream.svg')
        svg = xml.getroot()

        for glyph in findGlyph(letter, svg):

            if glyph.get("d") != None:
                string_d = glyph.get("d")

				## print string_d
                sp = "M"
                contours =  [sp+e for e in string_d.split(sp) if e]
                glyph_path_arr= []
                for contour in contours:
                    glyph_path_arr.append(parse_path(contour))

                ## modify your letter
                ## insert function etc.
                ## you have to return an array of non empty path()
                #####
                new_contours = magnetFontarr(glyph_path_arr, glyph, magnet_array[i], svg)


                ## rebuild
                ####
                d_array =[]
                if  new_contours is not None:
                    for contour in new_contours:
                        if contour != Path() and contour is not None:
                            d_array.append(contour.d())

                ## quick dirty fix
                ### note close path in a better way ?
                ####
                d_array.append("z")

                ## reexport a path in svg readable path
                #####
                glyph.attrib["d"] = 'z'.join(d_array)

        font_face = svg.findall(".//{http://www.w3.org/2000/svg}font-face")
        font_face[0].attrib["font-family"] = 'Stream-'+str(magnet_array[i])
        ## save file in svg
        f = open('Stream-'+str(magnet_array[i])+'.svg', 'w')
        f.write(ET.tostring(svg, pretty_print=True).decode("utf-8"))
        f.close()



## None for all glyph
processFont(None, len(magnet_array))

#####
